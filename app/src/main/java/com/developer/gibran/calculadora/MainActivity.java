package com.developer.gibran.calculadora;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    private EditText etMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etMain = (EditText)findViewById(R.id.etMain);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void writeOnEditText(View view){
        Button btnPressed = (Button)view;
        etMain.setText(etMain.getText().toString() + btnPressed.getText().toString());
    }
    public void operar(View view){

        try{
            etMain.setText(String.valueOf(Calculadora.operar(etMain.getText().toString())));
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void clear(View view){
        etMain.setText("0");
    }
    public void dell(View view){
        if(etMain.getText().toString().length()>1){
            etMain.setText(etMain.getText().toString().substring(0, etMain.getText().toString().length()-1));
        }else etMain.setText("0");

    }
}
